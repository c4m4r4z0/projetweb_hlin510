<?php 
session_start();
require_once 'functions.php';
if(!empty($_POST))
{
	$errors=array();
	require_once 'link.php';
	/*EMAIL*/
	if(empty($_POST['email']))
	{
		$errors['email']="Votre Email n'est pas valide";
	}
	/*EMAIL*/
	/*PASSWORD*/
	if(empty($_POST['password']))
	{
		$errors['password']="Veuiller renseigner un mot de passe";
	}
	/*PASSWORD*/


	else
	{
		/*SELECTION*/
		$req=$pdo->prepare("SELECT * FROM users WHERE email = ?");
		$req->execute(array($_POST['email']));
		$user = $req->fetch();
		if (!password_verify($_POST['password'],$user->password))
		{
			$errors['email_password'] = "le mot de passe ne corespond pas à l'email.";
		}
		/*SELECTION*/
		if(empty($errors))
		{
			$_SESSION['id']=$user->id;
			$_SESSION['role']=$user->role;
			if($_SESSION['role']=="organisateur")
			{
				header('Location: organisateur/site.php');
				exit();
			}
			if($_SESSION['role']=="participant")
			{
				header('Location: participant/site.php');
				exit();
			}
			if($_SESSION['role']=="administrateur")
			{
				header('Location: administrateur/site.php');
				exit();

			}
		}

	}
}
?>

<?php require 'headers.php'; ?>
<!--FOND-->
<div class="block">
	<?php if(!empty($errors)): ?>
	<div class="alert alert-danger">
		<p>Veuiller remplir le formulaire et correctement</p>
		<ul>
			<?php foreach ($errors as $error) : ?>
				<li><?= $error; ?></li>
			<?php endforeach; ?>
			
		</ul>
	</div>
<?php endif; ?>
	<div class="fond">
		<img src="src/img/fond.jpg" alt="Un chateau" class="fond-image">
		<!--FORMCO-->
		<form action="" method="POST">
			<div class="fond-content2">
				<h1 class="title is-1">CONEXION</h1>
				<div class="field champs">
					<label class="label">Email</label>
					<p class="control has-icons-left has-icons-right">
						<input class="input" name="email" type="email" placeholder="Email">
						<span class="icon is-small is-left">
							<i class="fas fa-envelope"></i>
						</span>
						<span class="icon is-small is-right">
							<i class="fas fa-check"></i>
						</span>
					</p>
				</div>
				<div class="field champs">
					<label class="label">Mot de Passe</label>
					<p class="control has-icons-left">
						<input class="input" name="password" type="password" placeholder="Password">
						<span class="icon is-small is-left">
							<i class="fas fa-lock"></i>
						</span>
					</p>
				</div>
				<div class="field champs">
					<p class="control">
						<button class="button is-success">Login</button>
					</p>
				</div>
			</div>
		</form>
		<!--FORMCO-->
	</div>
</div>
<!--FOND-->

<?php require 'footers.php'; ?>