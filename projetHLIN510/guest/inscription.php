<?php
require_once 'functions.php';
if(!empty($_POST))
{
	$errors=array();
	require_once 'link.php';
	/*NAME*/
	if(empty($_POST['name']) || !preg_match('/^[a-zA-Z]+$/',$_POST['name']))
	{
		$errors['name']="Votre NOM n'est pas valide";
	}
	/*NAME*/
	/*FIRST NAME*/
	if(empty($_POST['first_name']) || !preg_match('/^[a-zA-Z]+$/',$_POST['first_name']))
	{
		$errors['first_name']="Votre Prénom n'est pas valide";
	}
	/*FIRST NAME*/
	/*EMAIL*/
	if(empty($_POST['email']) || !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
	{
		$errors['email']="Votre Email n'est pas valide";
	}
	else
	{
		$req=$pdo->prepare("SELECT id FROM users WHERE email = ? ");
		$req->execute(array($_POST['email']));
		$user = $req->fetch();
		if ($user)
		{
			$errors['email'] = 'Cet email est déja pris.';
		}
	}
	/*EMAIL*/
	/*PASSWORD*/
	if(empty($_POST['password']) || $_POST['password']!=$_POST['password_confirm'])
	{
		$errors['password']="Votre Mot de passe n'est pas celui confirmer";
	}
	/*PASSWORD*/
	/*ROLE*/
	if(empty($_POST['role']))
	{
		$errors['role']="Veuiller selectionner un role please!";
	}
	/*ROLE*/
	if(empty($errors))
	{
		/*REQUETE*/
		$req = $pdo->prepare("INSERT INTO users SET name = ?, first_name = ?, password = ?, email = ?, role = ? ");
		$password = password_hash($_POST['password'],PASSWORD_BCRYPT);
		$req->execute(array($_POST['name'],$_POST['first_name'],$password,$_POST['email'],$_POST['role']));
		/*REQUETE*/
		header('Location: conexion.php');
		exit();
	}

}
?>

<?php require 'headers.php'; ?>

<!--FOND-->
<div class="block">
	<?php if(!empty($errors)): ?>
	<div class="alert alert-danger">
		<p>Veuiller remplir le formulaire et correctement</p>
		<ul>
			<?php foreach ($errors as $error) : ?>
				<li><?= $error; ?></li>
			<?php endforeach; ?>
			
		</ul>
	</div>
<?php endif; ?>
	
	<div class="fond">
		<img src="src/img/fond.jpg" alt="Un chateau" class="fond-image">
		<!--FORMIN-->
		<form action="" method="POST">
			<div class="fond-content2">
				<h1 class="title is-1">INSCRIPTION</h1>
				<div class="field is-horizontal">
					<label class="label">Voulez-vous vous inscrire en tant que Organisateur ou Participant ou Administrateur</label>
					<div class="field-body">
						<div class="field is-narrow">
							<div class="control">
								<label class="checkbox">
									<input type="radio" name="role" value="organisateur">
									<i class="fas fa-user-edit icon"></i>
								</label>
								<label class="checkbox">
									<input type="radio" name="role" value="participant">
									<i class="fas fa-smile-beam icon"></i>
								<label class="checkbox">
									<input type="radio" name="role" value="administrateur">
									<i class="fas fa-user-lock"></i>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="field champs">
		  			<label class="label">NOM</label>
		  			<div class="control">
		    			<input class="input" name="name" type="text" placeholder="ex: Smith">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Prénom</label>
		  			<div class="control">
		    			<input class="input" name="first_name" type="text" placeholder="ex: John">
		  			</div>
				</div>
				<div class="field champs">
					<label class="label">Email</label>
				 	<p class="control has-icons-left has-icons-right">
					    <input class="input" name="email" type="email" placeholder="Email">
					    <span class="icon is-small is-left">
					      	<i class="fas fa-envelope"></i>
					    </span>
					    <span class="icon is-small is-right">
					      	<i class="fas fa-check"></i>
					    </span>
				 	</p>
				</div>
				<div class="field champs">
					<label class="label">Mot de Passe</label>
					<p class="control has-icons-left">
						<input class="input" name="password" type="password" placeholder="Password">
						<span class="icon is-small is-left">
							<i class="fas fa-lock"></i>
						</span>
					</p>
				</div>
				<div class="field champs">
					<label class="label">Confirmer votre Mot de Passe</label>
					<p class="control has-icons-left">
						<input class="input" name="password_confirm" type="password" placeholder="Password">
						<span class="icon is-small is-left">
							<i class="fas fa-lock"></i>
						</span>
					</p>
				</div>
				<div class="field champs">
					<p class="control">
						<button class="button is-success">M'Inscrire</button>
					</p>
				</div>
			</div>
		</form>
		<!--FORMIN-->
	</div>
</div>
<!--FOND-->

<?php require 'footers.php'; ?>