<?php
try
{
	$pdo= new PDO('mysql:dbname=tuto;host=localhost','root','',
	array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_PERSISTENT => true,PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ));
}
catch(PDOException $e)
{
	echo $e->getMessage();
	die("Connexion impossible !");
}
