<?php 
require 'headers_admin.php';
require 'functions.php';
require_once 'link.php';
$sql = 'SELECT * FROM users WHERE
role != "administrateur" ORDER BY name';
if(isset($_GET['Supprimer']))
{
	$req=$pdo->prepare("DELETE FROM users WHERE email = ?");
	$req->execute(array($_GET['Supprimer']));
}
?>
<!--EVENEMENT-->
<div class="block">
	<div class="fond">
		<img src="src/img/fond.jpg" alt="Un chateau" class="fond-image">
		<div class="fond-content">
			<h1 class="title is-1">Gestion des Contributeur</h1>
		</div>
		<div class="tab-evenement">
			<table class="table is-striped is-bordered is-fullwidth">
				<thead>
					<tr>
						<th><br title="Nom-Contributeur">Nom</br></th>
						<th><br title="Prenom-Contributeur">Prénom</br></th>
						<th><br title="Email-Contributeur">Email</br></th>
						<th><br title="Role-Contributeur">Rôle</br></th>
						<th><br title="Selectionner-Contributeur">Selectionner</br></th>
					</tr>
				</thead>
				<tbody>
					<form action="" method="GET">
						<?php
						foreach($pdo->query($sql) as $row) 
						{
							print("<tr>");
							print("<th>".$row->name."</th>");
							print("<td>".$row->first_name."</td>");
							print("<td>".$row->email."</td>");
							print("<td>".$row->role."</td>");
							print("<td><button class='button is-success' name='Supprimer' value=".$row->email.">Supprimer</button></td>");
							print("</tr>");
						}
						?>
					</form>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!--EVENEMENT-->
<?php require 'footers.php'; ?>