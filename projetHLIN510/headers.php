<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<!-- <meta charset="iso-latin-1" / -->
		<title>Projet WEB</title>

		<!--CSS-->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
		<link rel="stylesheet" href="src/css/main.css">
		<!--CSS-->

		<!--FONT-->
		<link href="https://fonts.googleapis.com/css?family=PT+Serif:700&display=swap" rel="stylesheet">
		<script src="https://kit.fontawesome.com/f56a8cff1d.js" crossorigin="anonymous"></script>

		<!--FONT-->
	</head>
	<body>
		<!--MENUS-->
		<div class="block">
			<header class="menus">
				<a href="#" class="menus-logo">Mon projet</a>
				<nav class="menu">
					<a href="site.php">Acceuil</a>
					<a href="conexion.php">Conexion</a>
					<a href="inscription.php">Inscription</a>
					<a href="evenement.php">Evenement</a>
					<a href="#">A propos</a>
					<a href="#">Contact</a>
				</nav>
			</header>
		</div>
		<!--MENUS-->