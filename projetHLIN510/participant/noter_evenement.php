<?php 
require_once 'functions.php';
require 'headers_part.php';
require_once 'link.php';
$sql = 'SELECT * FROM assiste a, evenement e, lieu l WHERE
a.ide=e.id AND l.id=e.idl AND DATEDIFF( e.date_e, CURDATE()) <=0 ORDER BY nom';
if(isset($_GET['Noter']))
{
	$req=$pdo->prepare("SELECT * FROM evenement WHERE nom= ?");
	$req->execute(array($_GET['Noter']));
	$ev = $req->fetch();
	$_SESSION['Noter']=$ev->id;
	if($_GET['note']=='bien')
	{
		$note=100;
	}
	if($_GET['note']=='bof')
	{
		$note=50;
	}
	if($_GET['note']=='mauvais')
	{
		$note=0;
	}
	$req=$pdo->prepare("UPDATE assiste SET note = ? WHERE idp = ? AND ide = ?");
	$req->execute(array($note,$_SESSION['id'],$_SESSION['Noter']));
}
?>
<!--EVENEMENT-->
<!--EVENEMENT-->
<div class="block">
	<div class="fond">
		<img src="src/img/fond.jpg" alt="Un chateau" class="fond-image">
		<div class="fond-content">
			<h1 class="title is-1">Noter Mes Evenements</h1>
		</div>
		<div class="tab-evenement">
			<table class="table is-striped is-bordered is-fullwidth">
				<thead>
					<tr>
						<th><br title="Nom-Evenement">Nom</br></th>
						<th><br title="Cat-Evenement">Categorie</br></th>
						<th><br title="Loc-Evenement">Localisation</br></th>
						<th><br title="Horaire-Evenement">Horaire</br></th>
						<th><br title="Description-Evenement">Description</br></th>
						<th><br title="Selectionner-Evenement">Noter</br></th>
					</tr>
				</thead>
				<tbody>
					<form action="" method="GET">
						<?php
						foreach($pdo->query($sql) as $row) 
						{
							if($row->idp == $_SESSION['id'])
							{
								print("<tr>");
								print("<th>".$row->nom."</th>");
								print("<td>".$row->categorie."</td>");
								print("<td>".$row->adresse." ".$row->ville." ".$row->departement."</td>");
								print("<td>Le ".$row->date_e." à ".$row->heure." et dure ".$row->duree."</td>");
								print("<td>".$row->descriptif."</td>");
								print("<td><div class='field is-narrow'>
							<div class='control'>
								<label class='checkbox'>
									<input type='radio' name='note' value='bien'>
									<i class='fas fa-grin-alt'></i>
								</label>
								<label class='checkbox'>
									<input type='radio' name='note' value='bof'>
									<i class='fas fa-flushed'></i>
								<label class='checkbox'>
									<input type='radio' name='note' value='mauvais'>
									<i class='fas fa-angry'></i>
								</label>
							</div><button class='button is-success' name='Noter' value=".$row->nom.">Noter</button></td>");
								print("</tr>");
							}
						}
						?>
					</form>
					<tr>
						<th>Fiesta</th>
						<td>King of Creation</td>
						<td>Indeffinissable</td>
						<td>le ? à10h02 et dure 0</td>
						<td>Orgasma-Cosmico-Titanesque</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!--EVENEMENT-->
<?php require 'footers.php'; ?>