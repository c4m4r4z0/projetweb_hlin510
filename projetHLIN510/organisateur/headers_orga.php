<?php session_start();
if(!isset($_SESSION['id'])|| $_SESSION['role']!="organisateur")
{
	header('Location : https://webpeda.etu.umontpellier.fr/e20170009783/projet%20HLIN510/conexion.php');
	exit();
}

?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<!-- <meta charset="iso-latin-1" / -->
		<title>Projet WEB</title>

		<!--CSS-->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
		<link rel="stylesheet" href="src/css/main.css">
		<!--CSS-->

		<!--FONT-->
		<link href="https://fonts.googleapis.com/css?family=PT+Serif:700&display=swap" rel="stylesheet">
		<script src="https://kit.fontawesome.com/f56a8cff1d.js" crossorigin="anonymous"></script>

		<!--FONT-->
	</head>
	<body>
		<!--MENUS-->
		<div class="block">
			<header class="menus">
				<a href="#" class="menus-logo"><i class="fas fa-user-edit icon"></i>
				<nav class="menu">
					<a href="site.php">Acceuil</a>
					<a href="gere_evenement.php">Crée un Evenement</a>
					<a href="suppr_evenement.php">Supprimer mes Evenements</a>
					<a href="#">A propos</a>
					<a href="#">Contact</a>
					<a href="déconnection.php">Déconnection</a>
				</nav>
			</header>
		</div>
		<!--MENUS-->