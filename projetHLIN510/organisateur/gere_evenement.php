<?php 
require_once 'functions.php';
require 'headers_orga.php';
if(!empty($_POST))
{ 
	$errors=array();
	require_once 'link.php';
	/*NOM*/
	if(empty($_POST['name']) || empty($_POST['categorie']) || empty($_POST['name_l']) || empty($_POST['departement'])|| empty($_POST['ville'])|| empty($_POST['adresse'])|| empty($_POST['date'])|| empty($_POST['heure'])|| empty($_POST['duree'])|| empty($_POST['longitude'])|| empty($_POST['latitude'])|| empty($_POST['description']))
	{
		$errors['nom_ev']="Veuiller saisir toutes les infos";
	}
	else
	{
		$req=$pdo->prepare("SELECT id FROM evenement WHERE nom = ? ");
		$req->execute(array($_POST['name']));
		$ev = $req->fetch();
		if ($ev)
		{
			$errors['nom_ev'] = 'Ce nom est déja pris.';
		}
	}
	/*NOM*/
	if(empty($errors))
	{
		/*REQUETE*/
		$req = $pdo->prepare("INSERT INTO lieu SET departement = ?, ville = ?, adresse = ?, longitude = ?, latitude = ? ");
		$req->execute(array($_POST['departement'],$_POST['ville'],$_POST['adresse'],$_POST['longitude'],$_POST['latitude']));

		/*SELECTION*/
		$req=$pdo->prepare("SELECT * FROM lieu WHERE adresse = ?");
		$req->execute(array($_POST['adresse']));
		$ou = $req->fetch();
		$lieu=$ou->id;
		debug($_POST['categorie']);
		$req = $pdo->prepare("INSERT INTO evenement SET nom = ?, categorie = ?, date_e = ?, posteur = ?, idl = ?, tarif = ?, heure = ?, duree = ?, 	descriptif = ? ");
		$req->execute(array($_POST['name'],$_POST['categorie'],$_POST['date'],$_SESSION['id'],$lieu,$_POST['tarif'],$_POST['heure'],$_POST['duree'],$_POST['description']));
		/*REQUETE*/
		header('Location: gere_evenement.php');
		exit();

	}
}
?>

<!--FOND-->
<div class="block">
	<div class="fond">
		<img src="src/img/fond.jpg" alt="Un chateau" class="fond-image">
		<div class="fond-content2">
			<?php if(!empty($errors)): ?>
				<div class="notification is-danger">
					<p>Veuiller remplir le formulaire et correctement</p>
					<ul>
						<?php foreach ($errors as $error) : ?>
							<li><?= $error; ?></li>
						<?php endforeach; ?>
							
					</ul>
				</div>
			<?php endif; ?>	
			<h1 class="title is-1">Crée son Evenement</h1>
			<!--FORMIN-->
			<form action="" method="POST">
				<div class="field champs">
		  			<label class="label">NOM</label>
		  			<div class="control">
		    			<input class="input" name="name" type="text" placeholder="ex: Hellfest">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Catégorie</label>
		  			<div class="control">
		    			<input class="input" name="categorie" type="text" placeholder="ex: Hellfest">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Nom du lieu</label>
		  			<div class="control">
		    			<input class="input" name="name_l" type="text" placeholder="ex: Agora">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Département</label>
		  			<div class="control">
		    			<input class="input" name="departement" type="text">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Ville</label>
		  			<div class="control">
		    			<input class="input" name="ville" type="text">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Adresse</label>
		  			<div class="control">
		    			<input class="input" name="adresse" type="text" placeholder="30 Essex St Pascoe Vale, VIC 3044">
		  			</div>
				</div>
				<div class="field champs">
					<label class="label">Date</label>
					<div class="control">
						<input class="input" type="date" name="date">
					</div>
				</div>
				<div class="field champs">
		  			<label class="label">Heure</label>
		  			<div class="control">
		    			<input class="input" name="heure" type="text">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Durée</label>
		  			<div class="control">
		    			<input class="input" name="duree" type="text">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Longitude</label>
		  			<div class="control">
		    			<input class="input" name="longitude" type="text">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Latitude</label>
		  			<div class="control">
		    			<input class="input" name="latitude" type="text">
		  			</div>
				</div>
				<div class="field champs">
		  			<label class="label">Tarif</label>
		  			<div class="control">
		    			<input class="input" name="tarif" type="text">
		  			</div>
				</div>
				<div class="field">
					<label class="label">Description</label>
						<div class="control">
							<textarea class="textarea" name="description"></textarea>
					  	</div>
				</div>
				<div class="field champs">
					<p class="control">
						<button class="button is-success">Créer</button>
					</p>
				</div>

			</form>
			<!--FORMIN-->
		</div>
	</div>
</div>
<!--FOND-->


<?php require 'footers.php'; ?>