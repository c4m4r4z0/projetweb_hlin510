<?php 
require_once 'functions.php';
require 'headers_orga.php';
require_once 'link.php';
$sql = 'SELECT * FROM lieu l, evenement e WHERE
l.id=e.idl  AND DATEDIFF( e.date_e, CURDATE())>0 ORDER BY nom';
if(isset($_GET['Supprimer']))
{
	$req=$pdo->prepare("DELETE FROM evenement WHERE nom = ?");
	$req->execute(array($_GET['Supprimer']));
}
?>
<!--EVENEMENT-->
<!--EVENEMENT-->
<div class="block">
	<div class="fond">
		<img src="src/img/fond.jpg" alt="Un chateau" class="fond-image">
		<div class="fond-content">
			<h1 class="title is-1">Evenements</h1>
		</div>
		<div class="tab-evenement">
			<table class="table is-striped is-bordered is-fullwidth">
				<thead>
					<tr>
						<th><br title="Nom-Evenement">Nom</br></th>
						<th><br title="Cat-Evenement">Categorie</br></th>
						<th><br title="Loc-Evenement">Localisation</br></th>
						<th><br title="Horaire-Evenement">Horaire</br></th>
						<th><br title="Description-Evenement">Description</br></th>
						<th><br title="Selectionner-Evenement">Selectionner</br></th>
					</tr>
				</thead>
				<tbody>
					<form action="" method="GET">
						<?php
						foreach($pdo->query($sql) as $row) 
						{
							if($row->posteur == $_SESSION['id'])
							{
								print("<tr>");
								print("<th>".$row->nom."</th>");
								print("<td>".$row->categorie."</td>");
								print("<td>".$row->adresse." ".$row->ville." ".$row->departement."</td>");
								print("<td>Le ".$row->date_e." à ".$row->heure." et dure ".$row->duree."</td>");
								print("<td>".$row->descriptif."</td>");
								print("<td><button class='button is-success' name='Supprimer' value=".$row->nom.">Supprimer</button></td>");
								print("</tr>");
							}
						}
						?>
					</form>
					<tr>
						<th>Fiesta</th>
						<td>King of Creation</td>
						<td>Indeffinissable</td>
						<td>le ? à10h02 et dure 0</td>
						<td>Orgasma-Cosmico-Titanesque</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!--EVENEMENT-->
<?php require 'footers.php'; ?>