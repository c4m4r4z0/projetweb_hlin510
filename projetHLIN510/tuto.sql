﻿-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 17 déc. 2019 à 15:48
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tuto`
--

-- --------------------------------------------------------

--
-- Structure de la table `assiste`
--

CREATE TABLE `assiste` (
  `idp` int(15) NOT NULL,
  `ide` int(15) NOT NULL,
  `note` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `assiste`
--

INSERT INTO `assiste` (`idp`, `ide`, `note`) VALUES
(22, 3, '100');

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `id` int(15) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `date_e` date NOT NULL,
  `posteur` int(15) NOT NULL,
  `idl` int(15) NOT NULL,
  `tarif` decimal(6,0) NOT NULL,
  `heure` decimal(6,0) NOT NULL,
  `duree` decimal(6,0) NOT NULL,
  `descriptif` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`id`, `nom`, `categorie`, `date_e`, `posteur`, `idl`, `tarif`, `heure`, `duree`, `descriptif`) VALUES
(3, 'a', 'a', '2019-12-26', 22, 12, '0', '15', '12', 'aaaaa');

-- --------------------------------------------------------

--
-- Structure de la table `lieu`
--

CREATE TABLE `lieu` (
  `id` int(15) NOT NULL,
  `departement` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `longitude` decimal(6,0) NOT NULL,
  `latitude` decimal(6,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `lieu`
--

INSERT INTO `lieu` (`id`, `departement`, `ville`, `adresse`, `longitude`, `latitude`) VALUES
(8, 'd', 'd', 'd', '13', '1'),
(9, 'd', 'd', 'd', '13', '1'),
(10, 'd', 'd', 'd', '13', '1'),
(11, 'd', 'd', 'd', '13', '1'),
(12, 'q', 'q', '149 Rue Gay-Lussac Apt 24 Villa pétrarque', '13', '1'),
(13, 'a', 'a', '149 Rue Gay-Lussac Apt 24 Villa pétrarque', '0', '1');

-- --------------------------------------------------------

--
-- Structure de la table `organise`
--

CREATE TABLE `organise` (
  `idp` int(15) NOT NULL,
  `ide` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `email`, `password`, `role`) VALUES
(26, 'a', 'a', 'a@gmail.com', '$2y$10$ZhpzDZ7REOJkkpOZ9jstLOMNzeUY8sSDphoIyXVKkt5Lo1mLHj1vC', 'administrateur'),
(27, 's', 's', 's@gmail.com', '$2y$10$lcjF56NDkGOMb94tUJH2G.G2LTXoa8qyuC/vCGvgJhV0I1nCOQz7G', 'administrateur'),
(28, 'v', 'v', 'zaynox34@gmail.com', '$2y$10$zSvEIAWp2rstlxe5Sp.9Renwma7CbpFx4yPqIaZj6Jga/elpNKhhu', 'administrateur');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `assiste`
--
ALTER TABLE `assiste`
  ADD PRIMARY KEY (`idp`,`ide`);

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lieu`
--
ALTER TABLE `lieu`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `organise`
--
ALTER TABLE `organise`
  ADD PRIMARY KEY (`idp`,`ide`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `lieu`
--
ALTER TABLE `lieu`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
