<?php 
require_once 'functions.php';
require 'headers.php';
?>
<!--EVENEMENT-->
<div class="block">
	<div class="fond">
		<img src="src/img/fond.jpg" alt="Un chateau" class="fond-image">
		<div class="fond-content">
			<h1 class="title is-1">Evenements</h1>
		</div>
		<div class="tab-evenement">
			<table class="table is-striped is-bordered is-fullwidth">
				<thead>
					<tr>
						<th><br title="Nom-Evenement">Nom</br></th>
						<th><br title="Cat-Evenement">Categorie</br></th>
						<th><br title="Loc-Evenement">Localisation</br></th>
						<th><br title="Horaire-Evenement">Horaire</br></th>
						<th><br title="Description-Evenement">Description</br></th>
					</tr>
				</thead>
				<tbody>
					<?php
					require_once 'link.php';
					$sql = 'SELECT * FROM lieu l, evenement e WHERE
					l.id=e.idl AND DATEDIFF( e.date_e, CURDATE())>0 ORDER BY nom';
					foreach($pdo->query($sql) as $row) 
					{
						print("<tr>");
						print("<th>".$row->nom."</th>");
						print("<td>".$row->categorie."</td>");
						print("<td>".$row->adresse." ".$row->ville." ".$row->departement."</td>");
						print("<td>Le ".$row->date_e." à ".$row->heure." et dure ".$row->duree."</td>");
						print("<td>".$row->descriptif."</td>");
					}
					?>
					<tr>
						<th>Fiesta</th>
						<td>King of Creation</td>
						<td>Indeffinissable</td>
						<td>le ? à10h02 et dure 0</td>
						<td>Orgasma-Cosmico-Titanesque</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!--EVENEMENT-->
<?php require 'footers.php'; ?>
